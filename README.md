HOW TO:

1. Download, install and run glassfish appserver version 4.x or later

2. Create jdbc datasource named jdbc/GriiNgagelDS on the server by executing command line,

prompt> asadmin create-jdbc-resource --connectionpoolid MyPool jdbc/GriiNgagelDS


3. You may reuse the already existing jdbc connection pool 'mypool' above

4. Or you may create a new one, for example to create a new one for Apache Derby DBMS, execute command line,

prompt> asadmin create-jdbc-connection-pool --datasourceclassname org.apache.derby.jdbc.ClientDataSource --restype javax.sql.DataSource --property PortNumber=1527:Password=APP:User=APP:serverName=localhost:DatabaseName=melchior-db:URL=jdbc\\:derby\\://localhost\\:1527/melchior-db MyPool


5. Create user super@example.com with password=password, execute command line,

prompt> asadmin create-file-user super@example.com


6. Add the user just created above into group kms-admin and kms-user. Groups will be
   created on the fly in case do not yet exist. Execute command line,

prompt> asadmin update-file-user --groups kms-admin:kms-user super@example.com


7. Use mvn or netbeans to build project kms
   (the pom.xml is the top most in the source tree)
 
8. Deploy the resulting ear file (kms-ear/target/kms-ear-1.0-SNAPSHOT.ear) onto the glassfish server

9. Launch browser to http://localhost:8080/kms-web/faces/index.xhtml

10. Login using the user super@example.com
