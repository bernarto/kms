/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ses;

import com.mycompany.gui.rep.PersonRep;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author bernarto
 */
@Remote
public interface PersonControllerRemote {

    PersonRep deletePerson(Long id);

    Collection<PersonRep> fetchPersonsWithIds(Long[] ids);

    Collection<PersonRep> findPersonsByKeyword(String kw);

    PersonRep updatePerson(PersonRep rep);
}
