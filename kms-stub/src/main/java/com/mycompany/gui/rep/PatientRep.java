/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui.rep;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author bernarto
 */
public class PatientRep extends PersonRep implements Serializable {
    public enum Gender { Male, Female, Other };


    public PatientRep() {
    }
    public PatientRep(PersonRep p) {
        this.setId(p.getId());
        this.setFirstName(p.getFirstName());
        this.setLastName(p.getLastName());
        this.setAddresses(p.getAddresses());
        this.setContactNumber(p.getContactNumber());
        this.setDateOfBirth(p.getDateOfBirth());
    }



    public Boolean getIsOutPatient() {
        return _isOutPatient;
    }
    public void setIsOutPatient(Boolean outp) {
        _isOutPatient = outp;
    }
    private Boolean _isOutPatient;



    public Collection<PatientRecordRep> getPatientRecords() {
        return _patientRecords;
    }
    public void setPatientRecords(Collection<PatientRecordRep> recs) {
        _patientRecords = recs;
    }
    private Collection<PatientRecordRep> _patientRecords;



    public Gender getGender() {
        return _gender;
    }
    public void setGender(Gender g) {
        _gender = g;
    }
    private Gender _gender;



    public String getEmail() {
        return _email;
    }
    public void setEmail(String email) {
        _email = email;
    }
    private String _email;



    public double getHeight() {
        return _height;
    }
    public void setHeight(double height) {
        _height = height;
    }
    private double _height;



    public double getWeight() {
        return _weight;
    }
    public void setWeight(double weight) {
        _weight = weight;
    }
    private double _weight;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PatientRep)) {
            return false;
        }
        return super.equals(o);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.gui.rep.PatientRep[ id=" + ids + " ]";
    }


    @Override
    public char getPersonType() {
        return 'P';
    }
}
