/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui.rep;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author bernarto
 */
public class AddressRep implements Serializable {


    public Long getPerson() {
        return _person;
    }
    public void setPerson(Long p) {
        _person = p;
    }
    private Long _person;



    public Date getAdded() {
        return _added;
    }
    public void setAdded(Date t) {
        _added = t;
    }
    private Date _added;



    public String getUnitt() {
        return _unitt;
    }
    public void setUnitt(String u) {
        _unitt = u;
    }
    private String _unitt;



    public String getAddr1() {
        return _addr1;
    }
    public void setAddr1(String a) {
        _addr1 = a;
    }
    private String _addr1;



    public String getAddr2() {
        return _addr2;
    }
    public void setAddr2(String a) {
        _addr2 = a;
    }
    private String _addr2;



    public String getSuburb() {
        return _suburb;
    }
    public void setSuburb(String s) {
        _suburb = s;
    }
    private String _suburb;



    public String getCity() {
        return _city;
    }
    public void setCity(String ct) {
        _city = ct;
    }
    private String _city;



    public String getCountry() {
        return _country;
    }
    public void setCountry(String c) {
        _country = c;
    }
    private String _country;



    public Long getPostcode() {
        return _postcode;
    }
    public void setPostcode(Long code) {
        _postcode = code;
    }
    private Long _postcode;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = (_added != null) ? Long.hashCode(_added.getTime()) : 0;
        hash += (_person != null) ? _person.hashCode() : 0;
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AddressRep)) {
            return false;
        }
        AddressRep rhs = (AddressRep)o;
        return ((_added != null) ? _added.equals(rhs._added) : rhs._added == null) &&
               ((_person != null) ? _person.equals(rhs._person) : rhs._person == null);
    }


    @Override
    public String toString() {
        String person = (_person != null) ? _person.toString() : "[NULLPERSON]";
        String added = (_added != null) ? _added.toString() : "[NULLADDED]";
        return "com.mycompany.gui.rep.AddressRep[ _person=" + person + "|_added=" + added + " ]";
    }


    public int getNo() {
        return _no;
    }
    public void setNo(int no) {
        _no = no;
    }
    private int _no;
}
