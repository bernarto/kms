/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui.rep;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author bernarto
 */
public class StaffRep extends PersonRep implements Serializable {

    public StaffRep() {
    }
    public StaffRep(PersonRep p) {
        this.setId(p.getId());
        this.setFirstName(p.getFirstName());
        this.setLastName(p.getLastName());
        this.setAddresses(p.getAddresses());
        this.setContactNumber(p.getContactNumber());
        this.setDateOfBirth(p.getDateOfBirth());
    }



    public String getUserr() {
        return _userr;
    }
    public void setUserr(String usr) {
        _userr = usr;
    }
    private String _userr;



    public String getJobTitle() {
        return _jobTitle;
    }
    public void setJobTitle(String ttl) {
        _jobTitle = ttl;
    }
    private String _jobTitle;



    public Collection<PatientRecordRep> getPatientRecords() {
        return _patientRecords;
    }
    public void setPatientRecords(Collection<PatientRecordRep> recs) {
        _patientRecords = recs;
    }
    private Collection<PatientRecordRep> _patientRecords;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof StaffRep)) {
            return false;
        }
        return super.equals(o);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.gui.rep.StaffRep[ id=" + ids + " ]";
    }



    @Override
    public char getPersonType() {
        return 'S';
    }
}
