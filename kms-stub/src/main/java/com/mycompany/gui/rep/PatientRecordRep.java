/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui.rep;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @author bernarto
 */
public class PatientRecordRep implements Serializable {
    
    public Long getStaff() {
        return _staff;
    }
    public void setStaff(Long s) {
        _staff = s;
    }
    private Long _staff;



    public Long getPatient() {
        return _patient;
    }
    public void setPatient(Long p) {
        _patient = p;
    }
    private Long _patient;



    public Date getAdmission() {
        return _admission;
    }
    public void setAdmission(Date adm) {
        _admission = adm;
    }
    private Date _admission;



    public String getMedicalCond() {
        return _medicalCond;
    }
    public void setMedicalCond(String cond) {
        _medicalCond = cond;
    }
    private String _medicalCond;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = (_admission != null) ? Long.hashCode(_admission.getTime()) : 0;
        hash += (_staff != null ? _staff.hashCode() : 0);
        hash += (_patient != null ? _patient.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientRecordRep)) {
            return false;
        }
        PatientRecordRep rhs = (PatientRecordRep)o;
        return ((_admission != null) ? _admission.equals(rhs._admission) : rhs._admission == null) &&
               ((_staff != null) ? _staff.equals(rhs._staff) : rhs._staff == null) &&
               ((_patient != null) ? _patient.equals(rhs._patient) : rhs._patient == null);
    }


    @Override
    public String toString() {
        String staff = (_staff != null) ? _staff.toString() : "[NULLSTAFF]";
        String patient = (_patient != null) ? _patient.toString() : "[NULLPATIENT]";
        String admission = (_admission != null) ? _admission.toString() : "[NULLADMISSION]";
        return "com.mycompany.gui.rep.PatientRecordRep[ _staff=" + staff + "|_patient=" + patient + "|_admission=" + admission + " ]";
    }

}

