/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui.rep;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author bernarto
 */
public class PersonRep implements Serializable {

    public Long getId() {
        return _id;
    }
    public void setId(Long id) {
        _id = id;
    }
    protected Long _id;



    public String getFirstName() {
        return _firstName;
    }
    public void setFirstName(String nm) {
        _firstName = nm;
    }
    private String _firstName;



    public String getLastName() {
        return _lastName;
    }
    public void setLastName(String nm) {
        _lastName = nm;
    }
    private String _lastName;



    public String getDobStr() {
        String result = new String();
        if (_sdf != null && _dateOfBirth != null) {
            result = _sdf.format(_dateOfBirth);
        }
        return result;
    }
    public void setDobStr(String s) {
        if (_sdf != null && s != null) {
            try {
                _dateOfBirth = _sdf.parse(s);
            }
            catch( ParseException pe ) {
            }
        }
    }
    public Date getDateOfBirth() {
        return _dateOfBirth;
    }
    public void setDateOfBirth(Date dob) {
        _dateOfBirth = dob;
    }
    private Date _dateOfBirth;



    public String getContactNumber() {
        return _contactNumber;
    }
    public void setContactNumber(String num) {
        _contactNumber = num;
    }
    private String _contactNumber;



    public Collection<AddressRep> getAddresses() {
        return _addresses;
    }
    public void setAddresses(Collection<AddressRep> addr) {
        _addresses = addr;
    }
    private Collection<AddressRep> _addresses;


////////////////////////////////////////////////////////////////////////////////

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (_id != null ? _id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonRep)) {
            return false;
        }
        PersonRep rhs = (PersonRep)o;
        return (_id != null) ? _id.equals(rhs._id) : (rhs._id == null);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.gui.rep.PersonRep[ id=" + ids + " ]";
    }


////////////////////////////////////////////////////////////////////////////////


    public int getNo() {
        return _no;
    }
    public void setNo(int no) {
        _no = no;
    }
    private int _no;



    public boolean getIsSelected() {
        return _selected;
    }
    public void setIsSelected(boolean sel) {
        _selected = sel;
    }
    private boolean _selected;



    public char getPersonType() {
        return 'U';
    }



    public void setSdf(SimpleDateFormat sdf) {
        _sdf = sdf;
    }
    private SimpleDateFormat _sdf;
}
