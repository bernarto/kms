/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.web.bb;

import java.io.Serializable;

import com.mycompany.gui.rep.*;
import java.util.ArrayList;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;

import java.util.Collection;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;

import com.mycompany.ses.PersonControllerRemote;
import com.mycompany.web.ses.Current;
import com.mycompany.web.ses.AddressClient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author bernarto
 */
@Named
@SessionScoped
public class PersonCruds implements Serializable, AddressClient {

    @EJB
    private PersonControllerRemote _controller;


    @PostConstruct
    public void finalConstruct() {
        doReset2Staff();
    }



    public String doReset2Staff() {
        StaffRep s = new StaffRep();
        s.setDateOfBirth( zeroHMSm( new Date() ) );
        s.setSdf(sdf());
        setPerson( s );
        return doReset();
    }
    public String doReset2Patient() {
        PatientRep p = new PatientRep();
        p.setDateOfBirth( zeroHMSm( new Date() ) );
        p.setGender(PatientRep.Gender.Male);
        p.setIsOutPatient(false);
        p.setSdf(sdf());
        setPerson( p );
        return doReset();
    }
    public String doReset() {
        _pageSize = 20;
        _silent = true;
        _searchKeyword = (_searchKeyword == null) ? new String() : _searchKeyword;
        return doSearch();
    }



    public String doUpdate() {
        String msg = null;
        if ( !Current.getInstance().isUserInRole("kms-admin") ) {
            msg = "Error: insufficient privileges";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "person-cruds";
        }
        ArrayList<FacesMessage> fmliz = new ArrayList<FacesMessage>();
        String buf = _person.getLastName();
        if (buf == null || buf.length() <= 0) {
            msg = "Error: Lastname is a required field";
            fmliz.add(new FacesMessage(SEVERITY_ERROR, msg, msg));
        }
        if (_person.getDateOfBirth() == null) {
            msg = "Error: date of birth is a required field";
            fmliz.add(new FacesMessage(SEVERITY_ERROR, msg, msg));
        }
        buf = _person.getContactNumber();
        if (buf != null && buf.length() > 0) {
            buf = buf.trim();
            buf = buf.replaceAll("\\s", "");
            buf = buf.replaceAll("\\-", "");
            buf = buf.replaceAll("\\(", "");
            buf = buf.replaceAll("\\)", "");
            buf = buf.replaceAll("\\+", "");
            if (!buf.matches("[0-9]+") || buf.length() < 2 || buf.indexOf("+", 1) >= 0) {
                msg = "Error: contact number '" + _person.getContactNumber() +
                      "' may contain only numbers, minus (-) sign, spaces, opening/closing parentheses and must be at least 2 chars long";
                fmliz.add(new FacesMessage(SEVERITY_ERROR, msg, msg));
            }
            else {
                if (buf.charAt(0) == '0') {
                    buf = "62" + buf.substring( 1, buf.length() );
                }
                _person.setContactNumber(buf);
            }
        }
        if (fmliz.size() > 0) {
            FacesContext current = FacesContext.getCurrentInstance();
            for (FacesMessage fm : fmliz) {
                current.addMessage(null, fm);
            }
            return "person-cruds";
        }
        PersonRep person = null;
        try {
            person = _controller.updatePerson(_person);
        }
        catch(EJBException ejbex) {
        }
        if (person == null) {
            msg = "Error: server problem. Fail to create or update";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "person-cruds";
        }
        else {
            _person = person;
            if (_searchKeyword == null || _searchKeyword.length() <= 7) {
                _searchKeyword = "id:" + _person.getId();
            }
            if (_person.getPersonType() == 'S') {
                return doReset2Staff();
            }
            else if (_person.getPersonType() == 'P') {
                return doReset2Patient();
            }
            else {
                return doReset2Staff();
            }
        }
    }



    public String doDelete() {
        String msg = null;
        if ( !Current.getInstance().isUserInRole("kms-admin") ) {
            msg = "Error: insufficient privileges";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "person-cruds";
        }
        Long id = _person.getId();
        if (id == null || id <= 0L) {
            msg = "Error: a kontak must be selected first before deleting";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "person-cruds";
        }
        PersonRep person = null;
        try {
            person = _controller.deletePerson(id);
        }
        catch (EJBException ejbx) {
        }
        if (person == null) {
            msg = "Error: fail to delete Person with id=" + id +
                    ", could be deleted by another user already";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "person-cruds";
        }
        else {
            _person = person;
            if (_person.getPersonType() == 'S') {
                return doReset2Staff();
            }
            else if (_person.getPersonType() == 'P') {
                return doReset2Patient();
            }
            else {
                return doReset2Staff();
            }
        }
    }


    public String doSelect() {
        return "index";
    }



    public String doSelectPerson() {
        Object o = Current.getElement("p");
        if (o instanceof PersonRep) {
            PersonRep person = (PersonRep)o;
            setPerson( person );
        }
        return "person-cruds";
    }



    public String getSearchKeyword() {
        return _searchKeyword;
    }
    public void setSearchKeyword(String word) {
        _searchKeyword = word;
    }
    private String _searchKeyword;



    public Collection<PersonRep> getPersons() {
        return _persons;
    }
    private Collection<PersonRep> _persons;



    public String doSearch() {
        try {
            _persons = (_persons == null) ? new ArrayList<PersonRep>() : _persons;
            _persons.clear();
            String msg = null;
            if (_searchKeyword == null || _searchKeyword.length() < 4) {
                if (!_silent) {
                    msg = "Error: search keyword must be at least 4 characters long";
                    FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(SEVERITY_ERROR, msg, msg));
                }
                else {
                    _silent = false;
                }
                return "person-cruds";
            }
            try {
                _persons = _controller.findPersonsByKeyword(_searchKeyword);
            }
            catch (EJBException ejbex) {
            }

            if (_persons == null) {
                _persons = new ArrayList<>();
                msg = "Error: server problem. Fail to find Persons with keyword='"
                    + _searchKeyword + "'. Search result emptied";
                FacesContext.getCurrentInstance().addMessage(
                        null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            }
            else {
                int line = 0;
                for (PersonRep person : _persons) {
                    person.setNo( ++line );
                    person.setSdf( sdf() );
                }
            }
            return "person-cruds";
        }
        finally {
            while (_start > 0 && _start >= _persons.size()) {
                doPrevPage();
            }
        }
    }
    private boolean _silent;



    public String doAddressList() {
        Current.getInstance().setAddressClient(this);
        return "address-cruds";
    }



    @Override
    public String personDone(PersonRep person) {
        Current.getInstance().setAddressClient(null);
        return "person-cruds";
    }



    @Override
    public PersonRep getPerson() {
        return _person;
    }
    public void setPerson(PersonRep person) {
        _person = person;
    }
    private PersonRep _person;



    public StaffRep getStaff() {
        if ( getIsNonStaff() ) {
            if (_dummyStaff == null) {
                _dummyStaff = new StaffRep();
                _dummyStaff.setJobTitle("n/a");
                _dummyStaff.setPatientRecords(new ArrayList<PatientRecordRep>());
                _dummyStaff.setUserr("n/a");
            }
            return _dummyStaff;
        }
        else {
            return (StaffRep)_person;
        }
    }
    public boolean getIsNonStaff() {
        return _person.getPersonType() != 'S';
    }
    public PatientRep getPatient() {
        if ( getIsNonPatient() ) {
            if (_dummyPatient == null) {
                _dummyPatient = new PatientRep();
                _dummyPatient.setEmail("n/a");
                _dummyPatient.setPatientRecords(new ArrayList<PatientRecordRep>());
                _dummyPatient.setIsOutPatient(false);
                _dummyPatient.setGender(PatientRep.Gender.Other);
            }
            return _dummyPatient;
        }
        else {
            return (PatientRep)_person;
        }
    }
    public boolean getIsNonPatient() {
        return _person.getPersonType() != 'P';
    }
    private PatientRep _dummyPatient;
    private StaffRep _dummyStaff;



    public TimeZone getTimeZone() {
        if (_tz == null) {
            _tz = TimeZone.getTimeZone("GMT+07");
        }
        return _tz;
    }
    private TimeZone _tz;



    private SimpleDateFormat sdf() {
        if (_sdf == null) {
            _sdf = new SimpleDateFormat("dd MMM yyyy");
            _sdf.setTimeZone(getTimeZone());
        }
        return _sdf;
    }
    private SimpleDateFormat _sdf;



    private Date zeroHMSm(Date d) {
        cal().setTime(d);
        cal().set(Calendar.HOUR_OF_DAY, 0);
        cal().set(Calendar.MINUTE, 0);
        cal().set(Calendar.SECOND, 0);
        cal().set(Calendar.MILLISECOND, 0);
        return cal().getTime();
    }



    private Calendar cal() {
        if (_cal == null) {
            _cal = Calendar.getInstance( getTimeZone() );
        }
        return _cal;
    }
    private Calendar _cal;



    public String getPatientGender() {
        return getPatient().getGender().name();
    }
    public void setPatientGender(String str) {
        PatientRep.Gender g = PatientRep.Gender.valueOf(str);
        getPatient().setGender(g);
    }


    private SelectItem[] _possibleGenders;
    public SelectItem[] getPossibleGenders() {
        if (_possibleGenders == null) {
            _possibleGenders = new SelectItem[ PatientRep.Gender.values().length ];
            int i = 0;
            for (PatientRep.Gender g : PatientRep.Gender.values()) {
                _possibleGenders[i] = new SelectItem(new String(g.name()));
                _possibleGenders[i].setLabel(g.name());
                ++i;
            }
        }
        return _possibleGenders;
    }


////////////////////////////////////////////////////////////////////////////////


    public String getPageHint() {
        StringBuilder sb = new StringBuilder();
        int numOfPubs = (_persons != null) ? _persons.size() : 0;
        int windowSize = numOfPubs - _start;
        windowSize = (windowSize < _pageSize) ? windowSize : _pageSize;
        int start = (numOfPubs > 0) ? (_start + 1) : _start;
        sb.append("[").append(start).append("-").append(_start + windowSize).append("] of ").append(numOfPubs);
        return sb.toString();
    }

    public int getStart() {
        return _start;
    }
    private int _start;

    public int getPageSize() {
        return _pageSize;
    }
    private int _pageSize;

    public String doNextPage() {
        int numOfPubs = (_persons != null) ? _persons.size() : 0;
        int nextStart = _start + _pageSize;
        _start = (nextStart < numOfPubs) ? nextStart : _start;
        return "person-cruds";
    }
    public boolean getIsNextDisabled() {
        int numOfPubs = (_persons != null) ? _persons.size() : 0;
        int windowSize = numOfPubs - _start;
        return windowSize <= _pageSize;
    }

    public String doPrevPage() {
        int nextStart = _start - _pageSize;
        _start = (nextStart >= 0) ? nextStart : 0;
        return "person-cruds";
    }
    public boolean getIsPrevDisabled() {
        return _start <= 0;
    }

    public String doFirstPage() {
        _start = 0;
        return "person-cruds";
    }
    public String doLastPage() {
        while ( !getIsNextDisabled() ) {
            doNextPage();
        }
        return "person-cruds";
    }
}
