/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.web.bb;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import java.io.Serializable;
import java.util.Collection;

import com.mycompany.gui.rep.*;
import java.util.Date;
import javax.annotation.PostConstruct;
import java.util.TreeMap;

import com.mycompany.web.ses.*;

import java.util.ArrayList;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
/**
 *
 * @author bernarto
 */
@Named
@SessionScoped
public class AddressCruds implements Serializable {

    @PostConstruct
    public void finalConstruct() {
        doReset();
    }



    public String doReset() {
        _address = new AddressRep();
        _address.setAdded(new Date());
        return "address-cruds";
    }



    public String doDelete() {
        if (!getAddresses().contains(_address)) {
            String msg = "Error: address to be deleted must be from the table below";
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(SEVERITY_ERROR, msg, msg));
            return "address-cruds";
        }
        if (getAddresses().remove(_address)) {
            setPerson(_person);
        }
        return doReset();
    }



    public String doUpdate() {
        String msg = null;
        ArrayList<FacesMessage> fmliz = new ArrayList<>();
        String buf = _address.getCountry();
        if (buf == null || buf.length() <= 0) {
            msg = "Error: at least a country is required";
            fmliz.add(new FacesMessage(SEVERITY_ERROR, msg, msg));
        }
        if (fmliz.size() > 0) {
            FacesContext current = FacesContext.getCurrentInstance();
            for (FacesMessage fm : fmliz) {
                current.addMessage(null, fm);
            }
            return "address-cruds";
        }
        if (!getAddresses().contains(_address)) {
            getAddresses().add(_address);
            setPerson(_person);
        }
        return doReset();
    }



    public AddressRep getAddress() {
        return _address;
    }
    public void setAddress(AddressRep address) {
        _address = address;
    }
    private AddressRep _address;    



    public String doSelectAddress() {
        Object o = Current.getElement("a");
        if (o instanceof AddressRep) {
            AddressRep address = (AddressRep)o;
            setAddress( address );
        }
        return "address-cruds";
    }



    public Collection<AddressRep> getAddresses() {
        AddressClient client = Current.getInstance().getAddressClient();
        PersonRep person = (client != null) ? client.getPerson() : null;
        if (!_isShowingDetail && person != null) {
            _isShowingDetail = true;
            setPerson( person );
        }
        return (_person != null) ? _person.getAddresses() : null;
    }



    public PersonRep getPerson() {
        if (_person == null) {
            getAddresses();
        }
        return _person;
    }
    public void setPerson(PersonRep person) {
        if (person == null) {
            _person = person;
            return;
        }
        Collection<AddressRep> addresses = person.getAddresses();
        addresses = (addresses == null) ? new ArrayList<AddressRep>() : addresses;
        TreeMap<Long, AddressRep> sorter = new TreeMap<>();
        int i = 0;
        for (AddressRep address : addresses) {
            sorter.put(address.getAdded().getTime(), address);
            address.setNo( ++i );
        }
        try {
            addresses.clear();
        }
        finally {
            addresses.addAll( sorter.values() );
            person.setAddresses(addresses);
        }
        _person = person;
    }
    private PersonRep _person;



    public String doSelect() {
        try {
            AddressClient client = Current.getInstance().getAddressClient();
            if (client != null) {
                String prevpage = client.personDone(_person);
                setPerson( null );
                prevpage = (prevpage == null || prevpage.length() <= 0) ? "person-cruds" : prevpage;
                return prevpage;
            }
            else {
                return "person-cruds";
            }
        }
        finally {
            _isShowingDetail = false;
        }
    }
    private boolean _isShowingDetail;
}
