/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.web.bb;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import com.mycompany.web.ses.Current;
/**
 *
 * @author bernarto
 */
@Named
@RequestScoped
public class AccessControl {

    private static final String KMS_ADMIN = "kms-admin";
    private static final String KMS_USER = "kms-user";


    public boolean getUserIsAdmin() {
        if (_isAdmin == null) {
            _isAdmin = Current.getInstance().isUserInRole(KMS_ADMIN);
        }
        return _isAdmin;
    }
    private Boolean _isAdmin;


    public boolean getUserIsUser() {
        if (_isUser == null) {
            _isUser = Current.getInstance().isUserInRole(KMS_USER) || getUserIsAdmin();
        }
        return _isUser;
    }
    private Boolean _isUser;
    

    public String doLogout() {
        Current.getInstance().doLogout();
        Current.release();
        return "/logout.html";
    }
}
