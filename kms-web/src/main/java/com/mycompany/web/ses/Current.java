/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.web.ses;

import java.io.Serializable;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import java.io.Serializable;

/**
 *
 * @author bernarto
 */
public class Current implements Serializable {


    private Current() {
    }


    public static Object getElement(String name) {
        Object result = null;
        FacesContext current = FacesContext.getCurrentInstance();
        result = current.getExternalContext().getRequestMap().get(name);
        return result;
    }
    public static void setElement(String name, Object obj) {
        FacesContext current = FacesContext.getCurrentInstance();
        current.getExternalContext().getRequestMap().put(name, obj);
    }


    public static final String KEY = "kms.web.ses.Current";

    public static Current getInstance() {
        Current result = null;
        FacesContext current = FacesContext.getCurrentInstance();
        if (current != null) {
            ExternalContext external = current.getExternalContext();
            if (external != null) {
                Map<String, Object> map = external.getSessionMap();
                if (map != null) {
                    synchronized (map) {
                        result = (Current) map.get(KEY);
                        if (result == null) {
                            map.put(KEY, result = new Current());
                        }
                    }
                }        
            }            
        }
        return result;
    }


    public static Current release() {
        Current result = null;
        FacesContext current = FacesContext.getCurrentInstance();
        if (current != null) {
            ExternalContext external = current.getExternalContext();
            if (external != null) {
                Map<String, Object> map = external.getSessionMap();
                if (map != null) {
                    synchronized (map) {
                        result = (Current) map.get(KEY);
                        map.put(KEY, null);
                    }
                }
            }
        }
        return result;
    }


    public void doLogout() {
        FacesContext current = FacesContext.getCurrentInstance();
        if (current != null) {
            ExternalContext external = current.getExternalContext();
            if (external != null) {
                external.invalidateSession();
            }
        }
    }


    public String getUsername() {
        String result = null;
        FacesContext current = FacesContext.getCurrentInstance();
        if (current != null) {
            ExternalContext external = current.getExternalContext();
            if (external != null) {
                Object o = external.getRequest();
                if (o != null && (o instanceof HttpServletRequest)) {
                    HttpServletRequest request = (HttpServletRequest)o;
                    result = request.getRemoteUser();
                }
            }
        }
        return result;
    }


    public boolean isUserInRole(String role) {
        FacesContext current = FacesContext.getCurrentInstance();
        if (current != null) {
            ExternalContext external = current.getExternalContext();
            if (external != null) {
                Object o = external.getRequest();
                if (o != null && (o instanceof HttpServletRequest)) {
                    HttpServletRequest request = (HttpServletRequest)o;
                    return request.isUserInRole( role);
                }
            }
        }
        return false;
    }



    public AddressClient getAddressClient() {
        return _addressClient;
    }
    public void setAddressClient(AddressClient client) {
        _addressClient = client;
    }
    private AddressClient _addressClient;
}
