/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.web.ses;

import com.mycompany.gui.rep.*;


/**
 *
 * @author bernarto
 */
public interface AddressClient {
    PersonRep getPerson();
    String personDone(PersonRep person);
}
