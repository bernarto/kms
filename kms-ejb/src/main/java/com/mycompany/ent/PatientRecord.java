/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author bernarto
 */
@IdClass(PatientRecordKey.class)
@Entity
@Table(name = "KMS_PATIENT_REC")
@NamedQueries({
    @NamedQuery(
        name="findStaffRecords",
        query="SELECT r FROM PatientRecord r WHERE r.staff = :mystaff ORDER BY r.admission"),
    @NamedQuery(
        name="findPatientRecords",
        query="SELECT r FROM PatientRecord r WHERE r.patient = :mypatient ORDER BY r.admission")
})
public class PatientRecord implements Serializable {

    @NotNull
    @Id
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="STAFF_ID", referencedColumnName="ID")
    })
    public Staff getStaff() {
        return _staff;
    }
    public void setStaff(Staff s) {
        _staff = s;
    }
    private Staff _staff;



    @NotNull
    @Id
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="PATIENT_ID", referencedColumnName="ID")
    })
    public Patient getPatient() {
        return _patient;
    }
    public void setPatient(Patient p) {
        _patient = p;
    }
    private Patient _patient;



    @NotNull
    @Id
    public long getAdmission() {
        return _admission;
    }
    public void setAdmission(long adm) {
        _admission = adm;
    }
    private long _admission;



    public String getMedicalCond() {
        return _medicalCond;
    }
    public void setMedicalCond(String cond) {
        _medicalCond = cond;
    }
    private String _medicalCond;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = Long.hashCode(_admission);
        hash += (_staff != null) ? _staff.hashCode() : 0;
        hash += (_patient != null) ? _patient.hashCode() : 0;
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientRecord)) {
            return false;
        }
        PatientRecord rhs = (PatientRecord)o;
        return (_admission == rhs._admission)
                && ((_staff != null) ? _staff.equals(rhs._staff) : rhs._staff == null)
                && ((_patient != null) ? _patient.equals(rhs._patient) : rhs._patient ==  null);
    }


    @Override
    public String toString() {
        String staff = (_staff != null) ? _staff.toString() : "[NULLSTAFF]";
        String patient = (_patient != null) ? _patient.toString() : "[NULLPATIENT]";
        return "com.mycompany.ent.PatientRecord[ staff=" + staff + "|patient=" + patient + "|admission=" + _admission + " ]";
    }
    
}
