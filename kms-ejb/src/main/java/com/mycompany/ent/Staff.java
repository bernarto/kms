/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;
import java.util.Collection;
import static javax.persistence.CascadeType.ALL;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author bernarto
 */
@Entity
@Table(name = "KMS_STAFF")
public class Staff extends Person implements Serializable {
    private static final long serialVersionUID = 1L;


    public String getUserr() {
        return _userr;
    }
    public void setUserr(String usr) {
        _userr = usr;
    }
    private String _userr;



    public String getJobTitle() {
        return _jobTitle;
    }
    public void setJobTitle(String ttl) {
        _jobTitle = ttl;
    }
    private String _jobTitle;


    @OneToMany(cascade=ALL, mappedBy="staff", orphanRemoval=false)
    public Collection<PatientRecord> getPatientRecords() {
        return _patientRecords;
    }
    public void setPatientRecords(Collection<PatientRecord> recs) {
        _patientRecords = recs;
    }
    private Collection<PatientRecord> _patientRecords;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Staff)) {
            return false;
        }
        return super.equals(o);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.Staff[ id=" + ids + " ]";
    }
    
}
