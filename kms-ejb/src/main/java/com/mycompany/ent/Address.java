/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author bernarto
 */
@IdClass(AddressKey.class)
@Entity
@Table(name = "KMS_ADDRESS")
@NamedQueries({
    @NamedQuery(
        name="findPersonAddresses",
        query="SELECT a FROM Address a WHERE a.person = :myperson ORDER BY a.added")
})
public class Address implements Serializable {

    @NotNull
    @Id
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name="PERSON_ID", referencedColumnName="ID")
    })
    public Person getPerson() {
        return _person;
    }
    public void setPerson(Person p) {
        _person = p;
    }
    private Person _person;



    @NotNull
    @Id
    public long getAdded() {
        return _added;
    }
    public void setAdded(long t) {
        _added = t;
    }
    private long _added;



    public String getUnitt() {
        return _unitt;
    }
    public void setUnitt(String u) {
        _unitt = u;
    }
    private String _unitt;



    public String getAddr1() {
        return _addr1;
    }
    public void setAddr1(String a) {
        _addr1 = a;
    }
    private String _addr1;



    public String getAddr2() {
        return _addr2;
    }
    public void setAddr2(String a) {
        _addr2 = a;
    }
    private String _addr2;



    public String getSuburb() {
        return _suburb;
    }
    public void setSuburb(String s) {
        _suburb = s;
    }
    private String _suburb;



    public String getCity() {
        return _city;
    }
    public void setCity(String ct) {
        _city = ct;
    }
    private String _city;



    public String getCountry() {
        return _country;
    }
    public void setCountry(String c) {
        _country = c;
    }
    private String _country;



    public Long getPostcode() {
        return _postcode;
    }
    public void setPostcode(Long code) {
        _postcode = code;
    }
    private Long _postcode;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = Long.hashCode(_added);
        hash += (_person != null) ? _person.hashCode() : 0;
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        Address rhs = (Address)o;
        return (_added == rhs._added)
                && ((_person != null) ? _person.equals(rhs._person) : rhs._person == null);
    }


    @Override
    public String toString() {
        String person = (_person != null) ? _person.toString() : "[NULLPERSON]";
        return "com.mycompany.ent.Address[ person=" + person + "|added=" + _added + " ]";
    }
    
}
