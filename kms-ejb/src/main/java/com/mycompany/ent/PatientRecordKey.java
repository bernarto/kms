/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;

/**
 *
 * @author bernarto
 */
public class PatientRecordKey implements Serializable {


    public PatientRecordKey(Long staff, Long patient, long admission) {
        _staff = staff;
        _patient = patient;
        _admission = admission;
    }



    public Long getStaff() {
        return _staff;
    }
    public void setStaff(Long staff) {
        _staff = staff;
    }
    private Long _staff;



    public Long getPatient() {
        return _patient;
    }
    public void setPatient(Long p) {
        _patient = p;
    }
    private Long _patient;



    public long getAdmission() {
        return _admission;
    }
    public void setAdmission(long adm) {
        _admission = adm;
    }
    private long _admission;



////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = Long.hashCode(_admission);
        hash += (_staff != null ? _staff.hashCode() : 0);
        hash += (_patient != null ? _patient.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientRecordKey)) {
            return false;
        }
        PatientRecordKey rhs = (PatientRecordKey)o;
        return (_admission == rhs._admission)
            && ((_staff != null) ? _staff.equals(rhs._staff) : rhs._staff == null)
            && ((_patient != null) ? _patient.equals(rhs._patient) : rhs._patient == null);
    }


    @Override
    public String toString() {
        String staff = (_staff != null) ? _staff.toString(): "[NULLSTAFF]";
        String patient = (_patient != null) ? _patient.toString() : "[NULLPATIENT]";
        return "com.mycompany.ent.PatientRecordKey[ staff=" + staff
                + "| patient=" + patient  + "| admission=" + _admission + " ]";
    }
}
