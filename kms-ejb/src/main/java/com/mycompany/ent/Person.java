/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

import java.util.Collection;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.OneToMany;
/**
 *
 * @author bernarto
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "KMS_PERSON")
public abstract class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableGenerator(
        name = "personGen",
        table = "KMS_SEQUENCE_GENERATOR",
        pkColumnName = "GEN_KEY",
        valueColumnName = "GEN_VAL",
        pkColumnValue = "PERSON-ID",
        allocationSize = 10)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "personGen")
    public Long getId() {
        return _id;
    }
    public void setId(Long id) {
        _id = id;
    }
    protected Long _id;



    public String getFirstName() {
        return _firstName;
    }
    public void setFirstName(String nm) {
        _firstName = nm;
    }
    private String _firstName;


    @NotNull
    public String getLastName() {
        return _lastName;
    }
    public void setLastName(String nm) {
        _lastName = nm;
    }
    private String _lastName;



    public long getDateOfBirth() {
        return _dateOfBirth;
    }
    public void setDateOfBirth(long dob) {
        _dateOfBirth = dob;
    }
    private long _dateOfBirth;



    public String getContactNumber() {
        return _contactNumber;
    }
    public void setContactNumber(String num) {
        _contactNumber = num;
    }
    private String _contactNumber;


    @OneToMany(cascade=ALL, mappedBy="person", orphanRemoval=true)
    public Collection<Address> getAddresses() {
        return _addresses;
    }
    public void setAddresses(Collection<Address> addr) {
        _addresses = addr;
    }
    private Collection<Address> _addresses;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (_id != null ? _id.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person rhs = (Person) o;
        return (_id != null) ? _id.equals(rhs._id) : (rhs._id == null);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.Person[ id=" + ids + " ]";
    }
}
