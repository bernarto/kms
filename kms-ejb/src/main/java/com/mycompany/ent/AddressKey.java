/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import java.io.Serializable;
/**
 *
 * @author bernarto
 */
public class AddressKey implements Serializable {


    public AddressKey(Long person, long t) {
        _person = person;
        _added = t;
    }



    public Long getPerson() {
        return _person;
    }
    public void setPerson(Long p) {
        _person = p;
    }
    private Long _person;



    public long getAdded() {
        return _added;
    }
    public void setAdded(long t) {
        _added = t;
    }
    private long _added;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        int hash = Long.hashCode(_added);
        hash += (_person != null ? _person.hashCode() : 0);
        return hash;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AddressKey)) {
            return false;
        }
        AddressKey rhs = (AddressKey)o;
        return (_added == rhs._added)
            && ((_person != null) ? _person.equals(rhs._person) : rhs._person == null);
    }


    @Override
    public String toString() {
        String person = (_person != null) ? _person.toString() : "[NULLPERSON]";
        return "com.mycompany.ent.AddressKey[ person=" + person
                + "|added=" + _added  + " ]";
    }


}
