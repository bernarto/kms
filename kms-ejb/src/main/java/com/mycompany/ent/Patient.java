/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ent;

import com.mycompany.gui.rep.PatientRep;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Collection;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.OneToMany;

/**
 *
 * @author bernarto
 */
@Entity
@Table(name = "KMS_PATIENT")
public class Patient extends Person implements Serializable {
    private static final long serialVersionUID = 1L;


    public Boolean getIsOutPatient() {
        return _isOutPatient;
    }
    public void setIsOutPatient(Boolean outp) {
        _isOutPatient = outp;
    }
    private Boolean _isOutPatient;



    public PatientRep.Gender getGender() {
        return _gender;
    }
    public void setGender(PatientRep.Gender g) {
        _gender = g;
    }
    private PatientRep.Gender _gender;



    @OneToMany(cascade=ALL, mappedBy="patient", orphanRemoval=false)
    public Collection<PatientRecord> getPatientRecords() {
        return _patientRecords;
    }
    public void setPatientRecords(Collection<PatientRecord> recs) {
        _patientRecords = recs;
    }
    private Collection<PatientRecord> _patientRecords;



    public String getEmail() {
        return _email;
    }
    public void setEmail(String email) {
        _email = email;
    }
    private String _email;



    public double getHeight() {
        return _height;
    }
    public void setHeight(double height) {
        _height = height;
    }
    private double _height;



    public double getWeight() {
        return _weight;
    }
    public void setWeight(double weight) {
        _weight = weight;
    }
    private double _weight;


////////////////////////////////////////////////////////////////////////////////


    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Patient)) {
            return false;
        }
        return super.equals(o);
    }


    @Override
    public String toString() {
        String ids = (_id != null) ? _id.toString() : "[NULLID]";
        return "com.mycompany.Patient[ id=" + ids + " ]";
    }
}
