/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ses;

import javax.persistence.EntityManager;

import java.util.Date;
import java.util.Collection;
import java.util.ArrayList;

import com.mycompany.ent.*;
import com.mycompany.gui.rep.*;

/**
 *
 * @author bernarto
 */
public class X {

// Person
    public static Person r2e(PersonRep rep, EntityManager em) {
        Person result = null;
        if (rep == null) {
            return result;
        }

        if (rep.getPersonType() == 'S') {
            StaffRep sr = (StaffRep)rep;
            Staff s = new Staff();
            s.setJobTitle(sr.getJobTitle());
            s.setUserr(sr.getUserr());

            Collection<PatientRecordRep> patientRecordReps = sr.getPatientRecords();
            Collection<PatientRecord> patientRecords = null;
            if (patientRecordReps != null) {
                patientRecords = new ArrayList<>();
                for (PatientRecordRep patientRecordRep : patientRecordReps) {
                    PatientRecord patientRecord = X.r2e(patientRecordRep, em);
                    if (patientRecord != null) {
                        patientRecords.add(patientRecord);
                    }
                }
            }
            s.setPatientRecords(patientRecords);
            result = s;
        }
        else if (rep.getPersonType() == 'P') {
            PatientRep pr = (PatientRep)rep;
            Patient p = new Patient();
            p.setGender(pr.getGender());
            p.setIsOutPatient(pr.getIsOutPatient());
            p.setEmail(pr.getEmail());
            p.setHeight(pr.getHeight());
            p.setWeight(pr.getWeight());

            Collection<PatientRecordRep> patientRecordReps = pr.getPatientRecords();
            Collection<PatientRecord> patientRecords = null;
            if (patientRecordReps != null) {
                patientRecords = new ArrayList<>();
                for (PatientRecordRep patientRecordRep : patientRecordReps) {
                    PatientRecord patientRecord = X.r2e(patientRecordRep, em);
                    if (patientRecord != null) {
                        patientRecords.add(patientRecord);
                    }
                }
            }
            p.setPatientRecords(patientRecords);
            result = p;
        }
        else {
            return result;
        }
        result.setId(rep.getId());
        result.setFirstName(rep.getFirstName());
        result.setLastName(rep.getLastName());
        result.setContactNumber(rep.getContactNumber());

        Date dob = rep.getDateOfBirth();
        result.setDateOfBirth( (dob != null) ? dob.getTime() : 0L);

        Collection<AddressRep> addressReps = rep.getAddresses();
        Collection<Address> addresses = null;
        if (addressReps != null) {
            addresses = new ArrayList<>();
            for (AddressRep addressRep : addressReps) {
                Address address = X.r2e(addressRep, em);
                if (address != null) {
                    addresses.add(address);
                }
            }
        }
        result.setAddresses(addresses);
        return result;
    }
    public static PersonRep e2r(Person ent) {
        PersonRep result = null;
        if (ent == null) {
            return result;
        }
        result = new PersonRep();
        result.setId(ent.getId());
        result.setFirstName(ent.getFirstName());
        result.setLastName(ent.getLastName());
        result.setContactNumber(ent.getContactNumber());
        result.setDateOfBirth( new Date(ent.getDateOfBirth()) );

        Collection<Address> addresses = ent.getAddresses();
        Collection<AddressRep> addressReps = null;
        if (addresses != null) {
            addressReps = new ArrayList<>();
            for (Address address : addresses) {
                AddressRep addressRep = X.e2r(address);
                if (addressRep != null) {
                    addressReps.add( addressRep );
                }
            }
        }
        result.setAddresses(addressReps);

        if (ent instanceof Staff) {
            Staff s = (Staff)ent;

            StaffRep sr = new StaffRep(result);
            sr.setJobTitle(s.getJobTitle());
            sr.setUserr(s.getUserr());

            Collection<PatientRecord> patientRecords = s.getPatientRecords();
            Collection<PatientRecordRep> patientRecordReps = null;
            if (patientRecords != null) {
                patientRecordReps = new ArrayList<>();
                for (PatientRecord patientRecord : patientRecords) {
                    PatientRecordRep patientRecordRep = X.e2r( patientRecord );
                    if (patientRecordRep != null) {
                        patientRecordReps.add( patientRecordRep );
                    }
                }
            }
            sr.setPatientRecords(patientRecordReps);
            result = sr;
        }
        else if (ent instanceof Patient) {
            Patient p = (Patient)ent;

            PatientRep pr = new PatientRep(result);
            pr.setGender(p.getGender());
            pr.setIsOutPatient(p.getIsOutPatient());
            pr.setEmail(p.getEmail());
            pr.setHeight(p.getHeight());
            pr.setWeight(p.getWeight());

            Collection<PatientRecord> patientRecords = p.getPatientRecords();
            Collection<PatientRecordRep> patientRecordReps = null;
            if (patientRecords != null) {
                patientRecordReps = new ArrayList<>();
                for (PatientRecord patientRecord : patientRecords) {
                    PatientRecordRep patientRecordRep = X.e2r( patientRecord );
                    if (patientRecordRep != null) {
                        patientRecordReps.add( patientRecordRep );
                    }
                }
            }
            pr.setPatientRecords( patientRecordReps );
            result = pr;
        }
        return result;
    }



// PatientRecord
    public static PatientRecord r2e(PatientRecordRep rep, EntityManager em) {
        PatientRecord result = null;
        if (rep == null) {
            return result;
        }
        result = new PatientRecord();
        Long sid = rep.getStaff();
        result.setStaff( (sid != null) ? em.find(Staff.class, sid) : null );

        Long pid = rep.getPatient();
        result.setPatient((pid != null) ? em.find(Patient.class, pid) : null);

        Date a = rep.getAdmission();
        result.setAdmission(( a != null) ? a.getTime() : 0L );

        result.setMedicalCond(rep.getMedicalCond());
        return result;
    }
    public static PatientRecordRep e2r(PatientRecord ent) {
        PatientRecordRep result = null;
        if (ent == null) {
            return result;
        }
        result = new PatientRecordRep();

        Staff s = ent.getStaff();
        result.setStaff( (s != null) ? s.getId() : null );

        Patient p = ent.getPatient();
        result.setPatient( (p != null) ? p.getId() : null );

        result.setAdmission( new Date( ent.getAdmission() ) );

        result.setMedicalCond(ent.getMedicalCond());
        return result;
    }



// Address
    public static AddressRep e2r(Address ent) {
        AddressRep result = null;
        if (ent == null) {
            return result;
        }
        result = new AddressRep();

        Person p = ent.getPerson();
        result.setPerson((p != null) ? p.getId() : null);
        result.setAdded( new Date( ent.getAdded() ) );
        result.setAddr1(ent.getAddr1());
        result.setAddr2(ent.getAddr2());
        result.setCity(ent.getCity());
        result.setCountry(ent.getCountry());
        result.setSuburb(ent.getSuburb());
        result.setUnitt(ent.getUnitt());
        result.setPostcode(ent.getPostcode());
        return result;
    }
    public static Address r2e(AddressRep rep, EntityManager em) {
        Address result = null;
        if (rep == null) {
            return result;
        }
        result = new Address();

        Long pid = rep.getPerson();
        result.setPerson( (pid != null) ? em.find(Person.class, pid) : null );

        Date d = rep.getAdded();
        result.setAdded( (d != null) ? d.getTime() : 0L );

        result.setAddr1(rep.getAddr1());
        result.setAddr2(rep.getAddr2());
        result.setCity(rep.getCity());
        result.setCountry(rep.getCountry());
        result.setSuburb(rep.getSuburb());
        result.setUnitt(rep.getUnitt());
        result.setPostcode(rep.getPostcode());
        return result;
    }
}
