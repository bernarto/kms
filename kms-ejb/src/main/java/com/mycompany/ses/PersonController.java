/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ses;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.Collection;
import java.util.ArrayList;


import com.mycompany.gui.rep.*;
import com.mycompany.ent.*;

import com.mycompany.search.PersonIndexer;
import com.mycompany.search.PersonSearcher;


import java.io.IOException;
import java.util.TreeMap;
/**
 *
 * @author bernarto
 */
@Stateless
public class PersonController implements PersonControllerRemote {

    @Resource
    private SessionContext _context;

    @PersistenceContext(unitName = "kms-pu")
    private EntityManager _em;



    @Override
    public Collection<PersonRep> fetchPersonsWithIds(Long[] ids) {
        Collection<PersonRep> result = null;
        if (ids == null) {
            return result;
        }
        result = new ArrayList<>();
        for (Long id : ids) {
            PersonRep pr = X.e2r( (id != null) ? _em.find( Person.class, id ) : null );
            if (pr != null) {
                result.add( pr );
            }
        }
        return result;
    }



    @Override
    public Collection<PersonRep> findPersonsByKeyword(String kw) {
        Collection<PersonRep> result = new ArrayList<>();
        if (kw == null || kw.length() < 4) {
            return result;
        }
        try {
            Long[] ids = searcher().search( kw );
            result = fetchPersonsWithIds( ids );
        }
        catch (IOException ioe) {
        }
        if (result != null) {
            TreeMap<String, PersonRep> sorter = new TreeMap<>();
            for (PersonRep pr : result) {
                Long id = pr.getId();
                String sid = (id != null) ? id.toString() : new String();
                String key = pr.getLastName() + sid;
                sorter.put( key, pr );
            }
            result.clear();
            result.addAll( sorter.values() );
        }
        return result;
    }



    @Override
    public PersonRep deletePerson(Long id) {
        PersonRep result = null;
        Person ent = (id != null) ? _em.find( Person.class, id ) : null;
        if (ent == null) {
            return result;
        }
        result = X.e2r( ent );
        if (result != null) {
            _em.remove( ent );
            try {
                indexer().deleteFromIndex( id );
            }
            catch (IOException ioe) {
            }
        }
        return result;
    }



    @Override
    public PersonRep updatePerson(PersonRep rep) {
        PersonRep result = null;
        Person ent = X.r2e(rep, _em);
        if (ent == null) {
            return result;
        }

        Collection<Address> addresses = ent.getAddresses();
        ent.setAddresses(null);
        Long id = rep.getId();

        Collection<PatientRecord> patientRecords = null;
        Staff s = null;
        Patient p = null;
        if (ent instanceof Staff) {
            s = (Staff)ent;
            patientRecords = s.getPatientRecords();
            s.setPatientRecords(null);
        }
        else if (ent instanceof Patient) {
            p = (Patient)ent;
            patientRecords = p.getPatientRecords();
            p.setPatientRecords(null);
        }

        if (id == null || id <= 0L) {
            _em.persist(ent);
        }
        else {
//            _em.merge(ent);
        }

        if (addresses != null) {
            for (Address address : addresses) {
                if (address.getPerson() == null) {
                    address.setPerson(ent);
                    _em.persist(address);
                }
                else {
                    address.setPerson(ent);
                    _em.merge(address);
                }
            }
        }

        if (patientRecords != null) {
            for (PatientRecord patientRecord : patientRecords) {
                if (s != null && patientRecord.getStaff() == null) {
                    patientRecord.setStaff(s);
                    _em.persist(patientRecord);
                }
                else if (p != null && patientRecord.getPatient() == null) {
                    patientRecord.setPatient(p);
                    _em.persist(patientRecord);
                }
                else {
                    _em.merge(patientRecord);
                }
            }
        }
        ent.setAddresses(addresses);
        if (s != null) {
            s.setPatientRecords(patientRecords);
            _em.merge(s);
        }
        else if (p != null) {
            p.setPatientRecords(patientRecords);
            _em.merge(p);
        }
        result = X.e2r(ent);
        if (result != null) {
            try {
                if (id == null || id <= 0L) {
                    indexer().addToIndex( result.getId(), result.getFirstName(),
                            result.getLastName(), result.getContactNumber(),
                            result.getDateOfBirth(),
                            composeAddresses( result.getAddresses() ) );
                }
                else {
                    indexer().updateInIndex( result.getId(), result.getFirstName(),
                            result.getLastName(), result.getContactNumber(),
                            result.getDateOfBirth(),
                            composeAddresses( result.getAddresses() ) );
                }
            }
            catch(IOException ioe) {
            }
        }
        return result;
    }



    private String composeAddresses(Collection<AddressRep> addressReps) {
        String result = null;
        if (addressReps == null) {
            return result;
        }
        final String empty = new String();
        final String space = new String(" ");
        StringBuilder sb = new StringBuilder();
        for (AddressRep addressRep : addressReps) {

            if (addressRep == null) {
                continue;
            }
            String buf = addressRep.getAddr1();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            buf = addressRep.getAddr2();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            buf = addressRep.getCity();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            buf = addressRep.getCountry();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            buf = addressRep.getSuburb();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            buf = addressRep.getUnitt();
            buf = (buf == null) ? empty : buf;
            sb.append(buf).append(space);

            Long postcode = addressRep.getPostcode();
            if (postcode != null) {
                sb.append(postcode).append(space);
            }
        }
        result = sb.toString();
        return result;
    }



    private  PersonSearcher searcher() {
        if (_searcher == null) {
            _searcher = new PersonSearcher();
        }
        return _searcher;
    }
    private PersonSearcher _searcher;



    private PersonIndexer indexer() {
        if (_indexer == null) {
            _indexer = new PersonIndexer();
        }
        return _indexer;
    }
    private PersonIndexer _indexer;

}
