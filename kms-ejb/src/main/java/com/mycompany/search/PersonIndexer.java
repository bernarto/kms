/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.search;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import static org.apache.lucene.index.IndexWriterConfig.OpenMode.CREATE_OR_APPEND;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.util.Date;
/**
 *
 * @author bernarto
 */
public class PersonIndexer {

    public PersonIndexer(Path dir) {
        _indexDir = dir;
        _formater = new SimpleDateFormat("dd MMM yyyy HH:mm");
        _formater.setTimeZone(TimeZone.getTimeZone("GMT+07"));
    }


    public PersonIndexer() {
        String dirstr = Config._idxDir;
        if (dirstr != null && dirstr.length() > 0) {
            _indexDir = Paths.get(dirstr + File.separator + "person");
        }
        _formater = new SimpleDateFormat("dd MMM yyyy HH:mm");
        _formater.setTimeZone(TimeZone.getTimeZone("GMT+07"));
    }
    private Path _indexDir;
    private SimpleDateFormat _formater;


    public void addToIndex(Long id, String firstname, String lastname, String contact, Date dob, String addresses)
            throws IOException {

        if (id == null) {
            return;
        }
        final String empty = new String();
        firstname = (firstname == null) ? empty : firstname;
        lastname = (lastname == null) ? empty : lastname;
        contact = (contact == null) ? empty : contact;
        String datebuf = empty;
        if (dob != null) {
            datebuf = _formater.format(dob);
        }
        addresses = (addresses == null) ? empty : addresses;

        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);

            Document doc = new Document();
            doc.add( new LongField( "idlong", id, Field.Store.NO));
            doc.add( new StringField("id", id.toString(), Field.Store.YES));
            doc.add( new TextField( "firstname", new StringReader(firstname)) );
            doc.add( new TextField( "lastname", new StringReader(lastname)) );
            doc.add( new StringField("contact", contact, Field.Store.YES));
            doc.add( new TextField( "dob", new StringReader(datebuf)) );
            doc.add( new TextField( "addresses", new StringReader(addresses)) );

            indexWriter.addDocument(doc);
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }


    public void updateInIndex(Long id, String firstname, String lastname, String contact, Date dob, String addresses)
            throws IOException {

        if (id == null) {
            return;
        }
        final String empty = new String();
        firstname = (firstname == null) ? empty : firstname;
        lastname = (lastname == null) ? empty : lastname;
        contact = (contact == null) ? empty : contact;
        String datebuf = empty;
        if (dob != null) {
            datebuf = _formater.format(dob);
        }
        addresses = (addresses == null) ? empty : addresses;

        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);

            Document doc = new Document();
            doc.add( new LongField( "idlong", id, Field.Store.NO));
            doc.add( new StringField("id", id.toString(), Field.Store.YES));
            doc.add( new TextField( "firstname", new StringReader(firstname)) );
            doc.add( new TextField( "lastname", new StringReader(lastname)) );
            doc.add( new StringField("contact", contact, Field.Store.YES));
            doc.add( new TextField( "dob", new StringReader(datebuf)) );
            doc.add( new TextField( "addresses", new StringReader(addresses)) );

            indexWriter.updateDocument( new Term("id", id.toString()), doc);
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }


    public void deleteFromIndex(Long id) throws IOException {
        IndexWriter indexWriter = null;
        try {
            Directory dir = FSDirectory.open(_indexDir);
            Analyzer analyzer = new StandardAnalyzer();
            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
            iwc.setOpenMode(CREATE_OR_APPEND);
            indexWriter = new IndexWriter(dir, iwc);
            indexWriter.deleteDocuments(new Term("idlong", id.toString()) );
        }
        finally {
            if (indexWriter != null) {
                boolean closed = false;
                int counter = 0;
                do {
                    try {
                        indexWriter.close();
                        closed = true;
                    }
                    catch (Exception e) {
                        counter++;
                        if (counter >= 10) {
                            throw e;
                        }
                    }
                }
                while(!closed);
            }
        }
    }

}
